"use strict";

// Amikor az oldal betöltődik hívjuk meg a setupApp funkciót
window.addEventListener("load", setupApp);

// Rakjuk össze és indítsuk el az App-ot
function setupApp() {
  // Csináljunk egy error és három normál oldalt
  let errorPage = new Page("Error", false, "Error Page");
  let loginPage = new Page("Login", true, "Login Page");
  let forumPage = new Page("Forum", true, "Forum Page");
  let settingsPage = new Page("Settings", true, "Settings Page");

  // Csináljuk meg magát az App-ot
  let app = new App();

  // Adjuk hozzá az oldalakat az App-hoz
  app.addPage(errorPage);
  app.addPage(loginPage);
  app.addPage(forumPage);
  app.addPage(settingsPage);

  // Gyártsuk le az összes page és navItem HTML elemet
  app.render();

  // Adjunk egy event-listenert az ablakhoz, ha megváltozik a hash hívjuk meg az App onHshchange metódusát
  window.addEventListener("hashchange", app.onHashchange);

  // Ezzel a sorral manuálisan beállítom a Login hash-t, ami viszont aktiválni fogja az első onHashchange-t
  // ami pedig aktiválni fogja a Login page, és navItem elemeket, így kvázi "beröffentettem" az App-ot
  window.location.hash = "Login";
};
