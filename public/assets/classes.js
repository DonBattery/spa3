"use strict";

// A Page classból készült objektumok lesznek az oldalak
// a constructor funkciónak átadjuk az oldal nevét, egy booleant ami megmondja, hogy navigálható-e az oldal,
// és az oldal tartalmát (ami esetünkben még csak egy string, de később rakhatunk bele HTML elementeket is)
class Page {
  constructor(name, isNav, content) {
    this.name = name;
    this.isNav = isNav;
    this.content = content;
    // Ez a metódus visszaad egy #-t plusz az oldal nevét (Home -> #Home)
    this.hash = () => "#".concat(this.name);
    // Ez a metódus visszaadja az oldal nevét plusz Page (Home -> HomePage)
    this.pageId = () => this.name.concat("Page");
    // Ez a metódus visszaadja az oldal nevét plusz Button (Home -> HomeButton)
    this.navId = () => (this.isNav) ? this.name.concat("Button") : undefined;
  };
};

// Az App classból készült objektum-ot fogjuk arra használni, hogy kontrolláljuk a SPA működését
class App {
  constructor() {
    // Amikor lefut a konstruktor, elmentjük a NavBar és PageWrapper elemeket az oldalról az App objektum
    // navBar és pageWrapper propertyjeibe, később ezek alapján hivatkozhatunk rájuk
    this.navBar = document.getElementById("NavBar");
    this.pageWrapper = document.getElementById("PageWrapper");

    // Az App-nak van egy listája, ebben fogjuk tárolni az oldalakat
    this.pages = [];
    // Az addPage metódussal hozzá lehet adni egy oldalt az App pages listájához
    this.addPage = page => this.pages.push(page);
    // A getPageByHash visszaadja az adott hash-hoz tartozó oldalt, ha van ilyen
    this.getPageByHash = hash => this.pages.find(page => page.hash() == hash);

    // Muszály így "hozzá-bindelni" az App-hoz az onHashchange metódust, különben mikor
    // a Window-ra rakott event-listener triggereli, a "this" nem az App-ra fog mutatni,
    // hanem a Window-ra és az nekünk nem jó
    this.onHashchange = this.onHashchange.bind(this);
  };

  // A render metódus elkészítí a page és navItem HTML elementeket és felpakolja az oldalunkra
  render() {
    // végigmegyek az App pages listáján
    this.pages.forEach(page => {
      // minden page-val meghívom az App renderPage metódusát
      this.renderPage(page);
      // ha a page navItem, akkor meghívom vele az App renderNavForPage metódusát is
      if (page.isNav) {
        this.renderNavForPage(page);
      };
    });
  };

  // ez a metódus ellkészít és belerak egy page-t a PageWrapper-ba
  renderPage(page) {
    let newPage = document.createElement("div");
    newPage.classList.add("page", "basicBorder");
    newPage.id = page.pageId();
    newPage.innerHTML = page.content;
    this.pageWrapper.appendChild(newPage);
  };

  // ez a metódus elkészít és belerak egy navItemat a NavBar-ba
  renderNavForPage(page) {
    let newNavItem = document.createElement("div");
    newNavItem.classList.add("navItem", "basicBorder", "noSelect");
    newNavItem.id = page.navId();
    newNavItem.innerHTML = page.name;
    // ha ráklikkelenek a navItem-re változzon az oldal Hash-e a page.hash() értékére (Home -> #Home)
    newNavItem.addEventListener("click", () => window.location.hash = page.hash());
    this.navBar.appendChild(newNavItem);
  };

  // ez a metódus aktivál egy oldalat és ha navigálható az oldal akkor a hozzá tartozó navItem-et is aktiválja
  activatePage(nextPage) {
    // szedjük le az activePage classt az összes elemről, amin rajta van a page class
    document.querySelectorAll(".page").forEach(page => {
      page.classList.remove("activePage");
    });
    // rakjuk fel az activePage class-t arra az egy oldalra, amit paraméterül kapott ez a metódus (nextPage)
    document.getElementById(nextPage.pageId()).classList.add("activePage");

    // szedjük le az activeNavItem class-t az összes navItem-ről
    document.querySelectorAll(".navItem").forEach(navItem => {
      navItem.classList.remove("activeNavItem");
    });
    // ha a paraméterül kapott oldal (nextPage) navigálható, akkor aktiváljuk a hozzá tartozó navItemet
    // az activeNavItem class hozzáadásával
    if (nextPage.isNav) {
      document.getElementById(nextPage.navId()).classList.add("activeNavItem");
    };
  };

  // ez a metódus minden hash változásnál meg lesz hívva (window event-listener hashcahnge)
  onHashchange() {
    // nézzük meg hogy melyik oldalunk tartozik az aktuális hash-hoz
    let nextPage = this.getPageByHash(window.location.hash);
    // ha van ilyen, aktiváljuk
    if (nextPage) {
      this.activatePage(nextPage);
    } else {
      // ha nincs, cseréljük ki az ablak history-jában az aktuális oldalt,
      // az Error-page-re, és hívjuk meg újra ugyan ezt a metódust
      // ha csak simán átállítanám a window.location.hash-t Error-ra,
      // akkor egy végtelen ciklusba keveredne a böngésző
      window.history.replaceState({}, null, "#Error");
      this.onHashchange();
    };
  };
};
