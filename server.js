"use strict";

const express = require("express");
const morgan = require("morgan");
const app = express();
// Itt annyi a trükk, hogy ha van "PORT" nevű environment variable (környezeti változó)
// akkor PORT változónk annak az értékét fogja felvenni, ha nincs ilyen akkor 8080 lesz az értéke.
const PORT = process.env.PORT || 8080;

// Ezzel a sorral mondom meg, hogy az Appom használha a Morgan loggert, és a "combined" típusú logot kérek
app.use(morgan("combined"));

// A root-ról szolgáljuk ki a public mappa tartalmát
app.use("/", express.static("public"));

// Bármilyen más GET kérésre 404 lesz a válasz
app.get("*", (req, res) => {
  res.status(404).send("404 🐸 Not Found");
});

// Elindítom az Appot és kiírom a konzolra az URL-t amin elérhető
app.listen(PORT, () => {
  console.log(`SPA3 Server listening on http://localhost:${PORT}`);
});
